# Marketplace

### O Micro Serviço consiste em uma api Rest e como gerenciador de dependencias foi utilizado o Gradle


___
### H2 Database

Para armazenamento de dados em memória foi utilizado conexão com h2 database e ao startar a aplicação caso deseje ver como está a base, basta acessar:

http://localhost:8090/api/h2-console
O usuário e senha estão no aplication.properties na raiz do projeto. 
user: sa
password: 123

As entidades foram mapeadas de forma a criar automaticamente as entidades nessa base.

Essa base é startada junto com a API, ao ser startada a API o h2-console irá funcionar
___
### O projeto utiliza o Gradle como gerenciador de dependências

Nele se encontram a importação de bibliotecas como:
* Hibernate e JpaRepository para persistencia e conexao com a base de dados.
*  swagger para documentação de endpoints
*  H2 Database que serve para usarmos a base de dados em memória
*  Lombok para getters e setters, bem como equals, hash e toString das entidades 
*  javax.persistence para os mapeamentos das entidades

___
### Fluxo de criação de Cliente (Comprador)
```mermaid
sequenceDiagram

participant cliente as Cliente
participant sistema as Sistema
participant bd as BD

cliente ->> sistema: POST /client/create
sistema ->> sistema: Converte DTO em entidade
sistema ->> bd: save(client)
bd -->> sistema: 201 cerated << user >>
sistema -->> cliente: << user >>

```

___
### Fluxo de criação de Consultor (Vendedor)
```mermaid
sequenceDiagram

participant consultant as Consultant
participant sistema as Sistema
participant bd as BD

consultant ->> sistema: POST /consultant/create
sistema ->> sistema: Converte DTO em entidade
sistema ->> bd: save(consultant)
bd -->> sistema: 201 cerated << consultant >>
sistema -->> consultant: << consultant >>

```
___

### Fluxo de criação de Produto
```mermaid
sequenceDiagram

participant ator as Ator
participant sistema as Sistema
participant bd as BD

ator ->> sistema: POST /product/create
sistema ->> sistema: Converte DTO em entidade
sistema ->> bd: save(product)
bd -->> sistema: 201 cerated << product >>
sistema -->> ator: << product >>

```
___
### Fluxo de avaliação de um Produto
```mermaid
sequenceDiagram

participant ator as Ator
participant sistema as Sistema
participant bd as BD

ator ->> sistema: POST /evolution/create
sistema ->> sistema: Converte DTO em entidade
sistema ->> bd: save(sale)
bd -->> sistema: 201 cerated << sale >>
sistema -->> ator: << sale >>
```
___
### Fluxo de calculo de Score
```mermaid
sequenceDiagram

participant ator as Ator
participant sistema as Sistema
participant bd as BD

ator ->> sistema: GET /score
sistema ->> bd: getEvoluationsByProduct(productId)
bd --> sistema: << evoluation >>

alt dataRegistro > dataAtual -1 ano
    sistema ->> sistema: return soma += e.getScore()
end 

sistema ->> bd: findSaleByProduct(productId)
bd -->> sistema: << sales >>
sistema ->> sistema: return sales.size()

sistema ->> bd: findNews(productId)
bd -->> sistema: << news >>
alt dataRegistro == dataAtual
    sistema ->> sistema: return qtde++;
end

sistema ->> ator: return (ratings + qtdeSales + qtdeNews)

```
___
### Start da api

Ao iniciar a aplicação no config do projeto está configurado para criar as categorias e buscar as novidades delas no link indicado na descrição dessa atividade. 

__ 

### Colletions Postman

As collections encontram-se na raiz do projeto e podem ser importadas no Postman da seguinte forma:

Import >> IImportFile >> Chose Files >> Escolher arquivo postman_collection.json na raiz do projeto.

### Swagger 

Para acessar os endpoints pelo swagger basta acessar: http://localhost:8090/api/swagger-ui.html
