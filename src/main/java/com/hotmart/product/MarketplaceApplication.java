package com.hotmart.product;

import com.hotmart.product.model.entity.FirstNews;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

@SpringBootApplication
//@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
public class MarketplaceApplication {


    public static void main(String[] args) {
        SpringApplication.run(MarketplaceApplication.class, args);
    }


}
