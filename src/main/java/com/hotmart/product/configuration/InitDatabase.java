package com.hotmart.product.configuration;

import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeWithZoneIdSerializer;
import com.hotmart.product.model.entity.Articles;
import com.hotmart.product.model.entity.Category;
import com.hotmart.product.model.entity.CategoryNews;
import com.hotmart.product.model.entity.FirstNews;
import com.hotmart.product.model.enumerable.TypeEnum;
import com.hotmart.product.repository.CategoryNewsRepository;
import com.hotmart.product.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;


@Component
public class InitDatabase  {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryNewsRepository categoryNewsRepository;


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
         this.categoryRepository.saveAll(createCategory(restTemplate));
        return null;
    }

    private List<Category> createCategory(RestTemplate restTemplate){
        Category category;
        List<Category> categories = new ArrayList<>();

        List<String> categorias = new ArrayList<>();
        categorias.add(TypeEnum.business.toString());
        categorias.add(TypeEnum.entertainment.toString());
        categorias.add(TypeEnum.general.toString());
        categorias.add(TypeEnum.health.toString());
        categorias.add(TypeEnum.science.toString());
        categorias.add(TypeEnum.technology.toString());
        categorias.add(TypeEnum.sports.toString());

        for (int i = 0; i < 7; i++){
            category = new Category();
            category.setType(TypeEnum.valueOf(categorias.get(i)));

            FirstNews firtNews = restTemplate.getForObject(
                    "https://newsapi.org/v2/top-headlines?category="+categorias.get(i)+"&apiKey=8f6b52e0e516436e8e003fda3d2dc0dc", FirstNews.class);
            category.setCategoryNews(new HashSet<>(createNews(firtNews)));

            categories.add(category);
        }

        return categories;
    }

    private List<CategoryNews> createNews(FirstNews firstNews){
        Set<CategoryNews> news = new HashSet<>();
        CategoryNews categoryNews;
        Calendar aGMTCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

        for (Articles article: firstNews.getArticles()) {
            categoryNews = new CategoryNews();
            categoryNews.setDescription(article.getTitle());
            categoryNews.setRegistrationDate(aGMTCalendar.getTime());
            news.add(categoryNews);
        }

        return this.categoryNewsRepository.saveAll(news);
    }
}
