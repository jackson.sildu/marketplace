package com.hotmart.product.controller;

import com.hotmart.product.model.entity.CategoryNews;
import com.hotmart.product.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.List;

@Api(value = "Category")
@RestController
@RequestMapping(path = "/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    @ApiOperation(value = "Get news by category", notes = "Get news by category")
    public ResponseEntity<List<CategoryNews>> create(@RequestParam String categoryId) throws IOException {
        return ResponseEntity.ok(this.categoryService.findNews(categoryId));
    }
}
