package com.hotmart.product.controller;

//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
import com.hotmart.product.model.dto.ClientDTO;
import com.hotmart.product.model.entity.Client;
import com.hotmart.product.service.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Api(value = "Client")
@RestController
@RequestMapping(path = "/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/create")
    @ApiOperation(value = "Product Creation", notes = "Client creation method")
    public ResponseEntity<Client> create(@RequestBody ClientDTO clientDTO) throws IOException {
        return ResponseEntity.ok(this.clientService.create(clientDTO));
    }
}
