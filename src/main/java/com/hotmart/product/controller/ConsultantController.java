package com.hotmart.product.controller;

/*import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;*/
import com.hotmart.product.model.dto.ConsultantDTO;
import com.hotmart.product.model.entity.Consultant;
import com.hotmart.product.service.ConsultantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Api(value = "Consultant")
@RestController
@RequestMapping(path = "/consultant")
public class ConsultantController {
    @Autowired
    private ConsultantService consultantService;

    @PostMapping("/create")
    @ApiOperation(value = "Product Creation", notes = "Consultant creation method")
    public ResponseEntity<Consultant> create(@RequestBody ConsultantDTO consultantDTO) throws IOException {
        return ResponseEntity.ok(this.consultantService.create(consultantDTO));
    }
}
