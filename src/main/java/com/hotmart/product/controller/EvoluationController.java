package com.hotmart.product.controller;

import com.hotmart.product.model.dto.EvoluationDTO;
import com.hotmart.product.model.dto.ProductDTO;
import com.hotmart.product.model.entity.Evoluation;
import com.hotmart.product.service.EvoluationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Api(value = "Evoluation")
@RestController
@RequestMapping(path = "/evoluation")
public class EvoluationController {
    @Autowired
    private EvoluationService evoluationService;

    @PostMapping("/create")
    @ApiOperation(value = "Product Creation", notes = "Evoluation creation method")
    public ResponseEntity<Evoluation> create(@RequestBody EvoluationDTO evoluationDTO) throws IOException {
        return ResponseEntity.ok(this.evoluationService.save(evoluationDTO));
    }

    @GetMapping("/product-id")
    @ApiOperation(value = "Product Creation", notes = "Evoluation list method")
    public ResponseEntity<List<Evoluation>> list(@RequestParam String productId) throws IOException {
        return ResponseEntity.ok(this.evoluationService.getEvoluationsByProduct(productId));
    }
}
