package com.hotmart.product.controller;

import com.hotmart.product.model.dto.EvoluationDTO;
import com.hotmart.product.model.dto.ProductDTO;
import com.hotmart.product.model.entity.Evoluation;
import com.hotmart.product.model.entity.Product;
import com.hotmart.product.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Api(value = "Products Marketplace")
@RestController
@RequestMapping(path = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/create")
    @ApiOperation(value = "Product Creation", notes = "Product creation method")
    public ResponseEntity<Product> create(@RequestBody ProductDTO productDTO) throws IOException {
        return ResponseEntity.ok(this.productService.create(productDTO));
    }

    @GetMapping("/list")
    @ApiOperation(value = "Product List", notes = "Product list method")
    public ResponseEntity<List<Product>> get() throws IOException {
        return ResponseEntity.ok(this.productService.list());
    }

    @GetMapping("/id")
    @ApiOperation(value = "Product List", notes = "Product list method")
    public ResponseEntity<Optional<Product>> get(@RequestParam String id) throws IOException {
        return ResponseEntity.ok(this.productService.getById(id));
    }


}


