package com.hotmart.product.controller;

import com.hotmart.product.model.dto.SaleDTO;
import com.hotmart.product.model.entity.Sale;
import com.hotmart.product.service.SaleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.List;

@Api(value = "Sales")
@RestController
@RequestMapping(path = "/sale")
public class SaleController {
    @Autowired
    private SaleService saleService;

    @PostMapping("/create")
    @ApiOperation(value = "Product Creation", notes = "Sales creation method")
    public ResponseEntity<Sale> create(@RequestBody SaleDTO saleDTO) throws IOException {
        return ResponseEntity.ok(saleService.create(saleDTO));
    }

    @GetMapping("/product")
    @ApiOperation(value = "Product Creation", notes = "Sales creation method")
    public ResponseEntity<List<Sale>> getSaleByProduct(@RequestParam String productId) throws IOException {
        return ResponseEntity.ok(saleService.findSaleByProduct(productId));
    }
}
