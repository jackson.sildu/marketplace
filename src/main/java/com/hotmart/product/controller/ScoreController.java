package com.hotmart.product.controller;


import com.hotmart.product.service.ScoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;

@Api(value = "Score")
@RestController
@RequestMapping(path = "/score")
public class ScoreController {

    @Autowired
    private ScoreService scoreService;

    @GetMapping
    @ApiOperation(value = "Get score", notes = "Get score")
    public ResponseEntity<Double> getScore(@RequestParam String productId) throws IOException {
        return ResponseEntity.ok(this.scoreService.returnScore(productId));
    }
}
