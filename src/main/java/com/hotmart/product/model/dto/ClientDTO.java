package com.hotmart.product.model.dto;

import lombok.Getter;

@Getter
public class ClientDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
