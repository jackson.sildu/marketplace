package com.hotmart.product.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
public class ConsultantDTO {
    private String name;

    public String getName() {
        return name;
    }
}
