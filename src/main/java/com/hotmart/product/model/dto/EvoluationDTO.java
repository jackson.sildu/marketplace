package com.hotmart.product.model.dto;

public class EvoluationDTO {
    private String productId;
    private int score;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
