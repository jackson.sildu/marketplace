package com.hotmart.product.model.dto;

import com.hotmart.product.model.entity.Category;
import com.hotmart.product.model.entity.Evoluation;

import java.util.Set;

public class ProductDTO {

    private String name;
    private String description;
    private Set<Evoluation> evoluations;
    private String categoriaId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Evoluation> getEvoluations() {
        return evoluations;
    }

    public void setEvoluations(Set<Evoluation> evoluations) {
        this.evoluations = evoluations;
    }

    public String getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(String categoriaId) {
        this.categoriaId = categoriaId;
    }
}
