package com.hotmart.product.model.dto;

import com.hotmart.product.model.entity.Client;
import com.hotmart.product.model.entity.Product;

import java.util.List;
import java.util.Set;

public class SaleDTO {
    private String clientId;
    private String consultantId;
    private Product product;
    private Double totalPrice;
    private String categoriaId;

    public String getClientId() {
        return clientId;
    }

    public String getConsultantId() {
        return consultantId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public String getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(String categoriaId) {
        this.categoriaId = categoriaId;
    }


}
