package com.hotmart.product.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hotmart.product.model.enumerable.TypeEnum;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
@NoArgsConstructor
public class Category {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;

    @Enumerated(EnumType.ORDINAL)
    private TypeEnum type;

    @OneToMany(mappedBy = "category", fetch=FetchType.EAGER)
    private Set<CategoryNews> categoryNews;


    @OneToMany(mappedBy = "category", fetch=FetchType.EAGER)
    private List<Product> products;


}
