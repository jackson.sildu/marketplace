package com.hotmart.product.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class CategoryNews {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;
    private String description;
    private Date registrationDate;

    @JsonIgnore
    @ManyToOne
    private Category category;
}
