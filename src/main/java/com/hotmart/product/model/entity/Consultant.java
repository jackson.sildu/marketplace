package com.hotmart.product.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class Consultant {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;
    private String name;

}
