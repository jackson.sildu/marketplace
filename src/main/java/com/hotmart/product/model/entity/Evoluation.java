package com.hotmart.product.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter

@Entity(name = "Evoluation")
@Table(name = "product_evoluation")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class Evoluation {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;
    private int score;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    private Date registrationDate;
}
