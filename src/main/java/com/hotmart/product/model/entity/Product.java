package com.hotmart.product.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class Product {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;

    private String name;
    private String description;
    private Date registrationDate;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "product")
    private Set<Evoluation> evoluations = new HashSet<>();

    @JsonIgnore
    @ManyToOne
    private Category category;
}
