package com.hotmart.product.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@Entity
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class Sale {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;

    @ManyToOne(optional = false)
    @JoinColumn(name="product_id")
    private Product product;

    @ManyToOne(optional = false)
    @JoinColumn(name="consultant_id")
    private Consultant consultant;

    @ManyToOne(optional = false)
    @JoinColumn(name="client_id")
    private Client client;

    private Double totalPrice;
}
