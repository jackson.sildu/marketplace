package com.hotmart.product.model.enumerable;

public enum TypeEnum {
    business, entertainment, general, health, science, technology, sports;
}
