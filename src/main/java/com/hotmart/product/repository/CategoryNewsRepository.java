package com.hotmart.product.repository;

import com.hotmart.product.model.entity.Category;
import com.hotmart.product.model.entity.CategoryNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryNewsRepository extends JpaRepository<CategoryNews, String> {
    List<CategoryNews> findAllByCategory(Category category);
}
