package com.hotmart.product.repository;

import com.hotmart.product.model.entity.Evoluation;
import com.hotmart.product.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvoluationRepository extends JpaRepository<Evoluation, String> {
    List<Evoluation> findAllByProduct(Product product);
}
