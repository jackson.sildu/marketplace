package com.hotmart.product.repository;

import com.hotmart.product.model.entity.Evoluation;
import com.hotmart.product.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

   /* @Query("select a from Product a where a.registrationDate >= :date")
    Product findByBetweenDate(@Param("date") Date date);*/

}
