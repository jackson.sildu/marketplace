package com.hotmart.product.repository;

import com.hotmart.product.model.entity.Product;
import com.hotmart.product.model.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleRepository extends JpaRepository<Sale, String> {
    List<Sale> findSalesByProduct(Product product);
}
