package com.hotmart.product.service;

import com.hotmart.product.model.entity.Category;
import com.hotmart.product.model.entity.CategoryNews;
import com.hotmart.product.repository.CategoryNewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryNewsRepository categoryNewsRepository;

    public List<CategoryNews> findNews(String categoriaId){
        Category category = new Category();
        category.setId(categoriaId);
        return this.categoryNewsRepository.findAllByCategory(category);
    }

}
