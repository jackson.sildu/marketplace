package com.hotmart.product.service;

import com.hotmart.product.model.dto.ClientDTO;
import com.hotmart.product.model.entity.Client;
import com.hotmart.product.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public Client create(ClientDTO clientDTO){
        Client client = new Client();
        client.setName(clientDTO.getName());

        return this.clientRepository.save(client);
    }
}
