package com.hotmart.product.service;

import com.hotmart.product.model.dto.ConsultantDTO;
import com.hotmart.product.model.entity.Consultant;
import com.hotmart.product.repository.ConsultantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsultantService {
    @Autowired
    private ConsultantRepository consultantRepository;

    public Consultant create(ConsultantDTO consultantDTO){
        Consultant consultant = new Consultant();
        consultant.setName(consultantDTO.getName());
        return this.consultantRepository.save(consultant);
    }
}
