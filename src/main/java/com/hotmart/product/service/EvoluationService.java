package com.hotmart.product.service;

import com.hotmart.product.model.dto.EvoluationDTO;
import com.hotmart.product.model.entity.Evoluation;
import com.hotmart.product.model.entity.Product;
import com.hotmart.product.repository.EvoluationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@Service
public class EvoluationService {
    @Autowired
    private EvoluationRepository evoluationRepository;

    public Evoluation save(EvoluationDTO evoluationDTO){
        Evoluation evoluation = new Evoluation();
        Calendar aGMTCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

        Product product = new Product();
        product.setId(evoluationDTO.getProductId());

        evoluation.setProduct(product);
        evoluation.setScore(evoluationDTO.getScore());
        evoluation.setRegistrationDate(aGMTCalendar.getTime());

        return this.evoluationRepository.save(evoluation);
    }

    public List<Evoluation> getEvoluationsByProduct(String idProduto){
        Product product = new Product();
        product.setId(idProduto);
        return this.evoluationRepository.findAllByProduct(product);
    }
}
