package com.hotmart.product.service;

import com.hotmart.product.model.dto.ProductDTO;
import com.hotmart.product.model.entity.Category;
import com.hotmart.product.model.entity.Product;
import com.hotmart.product.repository.EvoluationRepository;
import com.hotmart.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EvoluationRepository evoluationRepository;

    public Product create(ProductDTO productDTO){
        return this.productRepository.save(convertDTO(productDTO));
    }

    public Product convertDTO(ProductDTO productDTO){
        Product product = new Product();
        product.setDescription(productDTO.getDescription());
        product.setName(productDTO.getName());

        Calendar aGMTCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        product.setRegistrationDate(aGMTCalendar.getTime());

        Category category = new Category();
        category.setId(productDTO.getCategoriaId());
        product.setCategory(category);

        return product;
    }


    public List<Product> list(){
        return this.productRepository.findAll();
    }

    public Optional<Product> getById(String id){
        return this.productRepository.findById(id);
    }

}
