package com.hotmart.product.service;

import com.hotmart.product.model.dto.SaleDTO;
import com.hotmart.product.model.entity.Client;
import com.hotmart.product.model.entity.Consultant;
import com.hotmart.product.model.entity.Product;
import com.hotmart.product.model.entity.Sale;
import com.hotmart.product.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
public class SaleService {

    @Autowired
    private SaleRepository saleRepository;

    public Sale create(SaleDTO saleDTO){
        return this.saleRepository.save(corvertDTO(saleDTO));
    }

    public Sale corvertDTO(SaleDTO saleDTO){

        Sale sale = new Sale();
        Client client = new Client();
        client.setId(saleDTO.getClientId());
        Consultant consultant = new Consultant();
        consultant.setId(saleDTO.getConsultantId());
        sale.setClient(client);
        sale.setConsultant(consultant);
        sale.setTotalPrice(saleDTO.getTotalPrice());
        sale.setProduct(saleDTO.getProduct());
        return sale;
    }

    public List<Sale> findSaleByProduct(String productId){
        Product product = new Product();
        product.setId(productId);
        return this.saleRepository.findSalesByProduct(product);
    }
}
