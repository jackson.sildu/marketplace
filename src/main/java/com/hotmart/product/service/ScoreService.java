package com.hotmart.product.service;

import com.hotmart.product.model.entity.CategoryNews;
import com.hotmart.product.model.entity.Evoluation;
import com.hotmart.product.model.entity.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Calendar;
import java.util.List;

@Service
public class ScoreService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SaleService saleService;

    @Autowired
    private EvoluationService evoluationService;

    public Double returnScore(String productId){

        Double ratings = this.returnRatings(productId);
        Double qtdeSales = this.returnQuantitySale(productId);
        Double qtdeNews = this.returnQtdeNews(productId);

        return (ratings + qtdeSales + qtdeNews);
    }

    public Double returnRatings(String productId){
        Integer soma = 0;
        Integer count = 0;

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);

        List<Evoluation> evoluations = this.evoluationService.getEvoluationsByProduct(productId);
        for(Evoluation e: evoluations){
            if(e.getRegistrationDate() != null && (e.getRegistrationDate().compareTo(calendar.getTime()) == 1)){
                soma += e.getScore();
                count++;
            }
        }

        return Double.valueOf((soma / count));
    }

    public Double returnQuantitySale(String productId){
        List<Sale> sales = this.saleService.findSaleByProduct(productId);
        return Double.valueOf(sales.size());
    }

    public Double returnQtdeNews(String productId){
        List<CategoryNews> news = this.categoryService.findNews(productId);
        Calendar calendar = Calendar.getInstance();
        Double qtde = Double.valueOf(0);
        for (CategoryNews n: news) {
            if(n.getRegistrationDate().compareTo(calendar.getTime()) == 0)
                qtde++;
        }

        return Double.valueOf(qtde);
    }
}
