package com.hotmart.product.model;

import com.hotmart.product.model.dto.ClientDTO;
import com.hotmart.product.model.dto.ProductDTO;
import com.hotmart.product.model.entity.Category;
import com.hotmart.product.model.entity.Client;
import com.hotmart.product.model.entity.Evoluation;
import com.hotmart.product.model.entity.Product;
import com.hotmart.product.model.enumerable.TypeEnum;

import java.util.*;

public class EntityDTO {

    public static ClientDTO getClientDTO(){
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setName("Gabriela");
        return clientDTO;
    }

    public static Client getClient(){
        Client client = new Client();
        client.setId("id");
        client.setName("Gabriela");
        return client;
    }

    public static Category getCategory(){
        Category category = new Category();
        category.setId("uid");
        category.setType(TypeEnum.business);
        return category;
    }

    public static Set<Evoluation> getEvoluations(){
        Evoluation evoluation = new Evoluation();
        evoluation.setRegistrationDate(new Date());
        evoluation.setScore(3);
        evoluation.setId("uid");

        Set<Evoluation> evoluations = new HashSet<>();
        evoluations.add(evoluation);
        return evoluations;
    }

    public static Product getProduct(){
        Product product = new Product();
        product.setId("uid");
        product.setCategory(getCategory());
        product.setRegistrationDate(new Date());
        product.setName("name");
        product.setDescription("descripion");
        product.setEvoluations(getEvoluations());
        return product;
    }

    public static ProductDTO getProductDTO(){
        ProductDTO product = new ProductDTO();
        product.setName("name");
        product.setDescription("descripion");
        Set<Evoluation> evoluations = new HashSet<>();
        product.setEvoluations(evoluations);
        product.setCategoriaId("teste");
        return product;
    }

}
