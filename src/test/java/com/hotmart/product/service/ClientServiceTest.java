package com.hotmart.product.service;

import com.hotmart.product.model.EntityDTO;
import com.hotmart.product.model.dto.ClientDTO;
import com.hotmart.product.model.entity.Client;
import com.hotmart.product.repository.ClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {
    @InjectMocks
    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;

    @Test
    public void create(){
        Client client = EntityDTO.getClient();
        ClientDTO clientDTO = EntityDTO.getClientDTO();

        Mockito.when(clientRepository.save(client)).thenReturn(client);
        Client response = clientService.create(clientDTO);
    }
}
